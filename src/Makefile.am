# Library versioning (C:R:A == current:revision:age)
#
# Various contradictory rules can be found for library versioning,
# but we shall follow these:
#
#   https://www.sourceware.org/autobook/autobook/autobook_61.html
#
# In summation,
#
#   * If the library source code has changed at all since the last
#     update, then we update the revision number.
#
#   * If any public interfaces have been added, removed, or changed
#     since the last update, we increment the "current" number, and
#     set the revision number to 0.
#
#   * If any interfaces have been added since the last public release,
#     then we increment the "age" number. If any interfaces have been
#     changed or removed, set the "age" to 0.
#
LIBSYMMETRICA_LT_VERSION = 2:0:0

# The top-level symmetrica.h is meant to be included by consumers; the
# original def.h and macro.h are thereby pulled in from the symmetrica
# subdirectory. This avoids name clashes from installing the generically-
# named def.h and macro.h in the global include path.
include_HEADERS = symmetrica.h
pkginclude_HEADERS = def.h macro.h

lib_LTLIBRARIES = libsymmetrica.la

# Always include the version script in the distribution tarball,
# even if it might not get used; we won't know that until later.
EXTRA_DIST = symmetrica.map

libsymmetrica_la_LD_VERSION_SCRIPT =
if HAVE_LD_VERSION_SCRIPT
libsymmetrica_la_LD_VERSION_SCRIPT += -Wl,--version-script=$(top_srcdir)/src/symmetrica.map

# Depend on symmetrica.map for the link step, so that changes to the
# map file will trigger a re-link of the library.
EXTRA_libsymmetrica_la_DEPENDENCIES = symmetrica.map
endif

# -DFAST disables some runtime checks in src/macro.h to speed things up.
#
# -DALLTRUE defines a bunch of other constants in def.h that ultimately
# enable every symmetrica "subsystem."
libsymmetrica_la_CPPFLAGS = -DFAST -DALLTRUE

libsymmetrica_la_CFLAGS =
if HAVE_WNO_UNUSED_RESULT
# The library consistently ignores the return values of things like
# scanf and fscanf, so it's counterproductive to emit a million
# warnings about it.
libsymmetrica_la_CFLAGS += -Wno-unused-result
endif

test_CFLAGS=
if HAVE_FNO_MS_COMPATIBILITY
# def.h defines its own min() and max() functions that clash with the
# ones defined in the Windows SDK's version of stdlib.h. The ones in
# stdlib.h are disabled when __STDC__ is defined truthy, but for
# bug-compatibility with MSVC, clang doesn't do that by default when
# it's targeting Windows. This flag, when it exists, disables
# compatibility mode in clang, and makes the whole endeavor succeed.
# In particular this is needed to compile using clang for Windows
# and the Windows SDK headers.
libsymmetrica_la_CFLAGS += -fno-ms-compatibility
test_CFLAGS += -fno-ms-compatibility
endif


libsymmetrica_la_SOURCES = \
	bar.c \
	bi.c \
	boe.c \
	bruch.c \
	classical.c \
	de.c \
	di.c \
	ff.c \
	galois.c \
	ga.c \
	gra.c \
	hash.c \
	hiccup.c \
	io.c \
	ko.c \
	list.c \
	lo.c \
	ma.c \
	mee.c \
	mem.c \
	mes.c \
	mhe.c \
	mhh.c \
	mhm.c \
	mhp.c \
	mhs.c \
	mmm.c \
	mms.c \
	mod_dg_sbd.c \
	mo.c \
	mpp.c \
	mps.c \
	mse.c \
	msh.c \
	msm.c \
	mss.c \
	muir.c \
	na.c \
	nb.c \
	nc.c \
	nu.c \
	part.c \
	pee.c \
	peh.c \
	pem.c \
	perm.c \
	pes.c \
	phe.c \
	phh.c \
	phm.c \
	phs.c \
	plet.c \
	pme.c \
	pmh.c \
	poly.c \
	ppe.c \
	pph.c \
	ppm.c \
	ppp.c \
	pps.c \
	pr.c \
	pse.c \
	psh.c \
	psm.c \
	pss.c \
	rest.c \
	rh.c \
	sab.c \
	sb.c \
	sc.c \
	sr.c \
	ta.c \
	teh.c \
	tem.c \
	tep.c \
	tes.c \
	the.c \
	thm.c \
	thp.c \
	ths.c \
	tme.c \
	tmh.c \
	tmp.c \
	tms.c \
	tpe.c \
	tph.c \
	tpm.c \
	tps.c \
	tse.c \
	tsh.c \
	tsm.c \
	tsp.c \
	vc.c \
	zo.c \
	zykelind.c \
	zyk.c

# The -no-undefined flag is required to create a Windows DLL,
# because that platform does not support shared libraries with
# undefined symbols in them. Symmetrica, however, does not rely
# on undefined symbols in the first place. So, we append the
# flag unconditionally.
libsymmetrica_la_LDFLAGS = \
	-version-info $(LIBSYMMETRICA_LT_VERSION) \
	$(libsymmetrica_la_LD_VERSION_SCRIPT) \
	-no-undefined

libsymmetrica_la_LIBADD = $(LIBM)


# The "test" executable itself.
test_CPPFLAGS = $(libsymmetrica_la_CPPFLAGS)
test_SOURCES = test.c
test_LDADD = libsymmetrica.la

# The "test" executable should be built only during "make check".
check_PROGRAMS = test

# Also include the test runner as necessary stuff for "make check".
check_SCRIPTS = run-tests.sh

# Finally, tell the build system what test suites exist.
TESTS = $(check_SCRIPTS)
